tyscca.eu
========================

Mettre en place son environnement de développement
--------------------------------------------------

### Cloner le dépôt git
	git clone git@bitbucket.org:openscop/escca.git
	git pull

### Installer les pacquets python de base
	sudo aptitude install python python-dev build-essential libpq-dev libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.5-dev tk8.5-dev python-tk

### Installer pip
	sudo aptitude install python-pip

### Installer virtualenv
	sudo pip install virtualenv

### Se placer à la racine du projet
	cd escca/

### Créer un virtualenv spécialement pour le projet
	virtualenv env
	source env/bin/activate

Cette dernière commande sera utile (et donc vous devrez l'executer) à chaque fois que l'on voudra travailler sur le projet

### Installer les dépendances requises par le projet
dans le fichier requirements.txt se trouvent les dépendances nécessaires à la plateforme

	pip install -r requirements.txt

### Création du fichier de configuration à l'aide du patron fourni
	cp escca/settings.sample.py escca/settings.py

### Migration initiale de la base de données
	python manage.py migrate
	python manage.py runserver_plus

### Test
L'application devrait être accessible à l'adresse suivante : http://localhost:8000
