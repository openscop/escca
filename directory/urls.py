from __future__ import unicode_literals

from django.conf.urls import url

from .views import IndexView
from .views import UserDetailView
from .views import UserListView

urlpatterns = [
    url(r'^(?P<letter>[A-Z])/$', UserListView.as_view(), name='user-list'),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)$', UserDetailView.as_view(), name='user-detail'),
]
