# List of applications tested when './manage.py jenkins' command
# https://github.com/kmmbvnr/django-jenkins/
PROJECT_APPS = (
    'cytometry',
    'directory',
    'quiz',
    'sponsors',
    'lectures',
    'tutorships',
)

# List of Jenkins tasks executed by './manage.py jenkins' command
JENKINS_TASKS = (
    'django_jenkins.tasks.run_sloccount',
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8',
)

PEP8_RCFILE = 'pep8.rc'

PYLINT_LOAD_PLUGIN = (
    'pylint_django',
)
