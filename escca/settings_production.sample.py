"""
Django settings for escca project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
from escca.settings import *


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['']

SITE_ID = 1

# Email backend
# https://docs.djangoproject.com/en/1.6/topics/email/#email-backends
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_USER = ''
EMAIL_USE_TLS = True


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'localhost',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATICFILES_DIRS = (
    '/home/admin/escca/env/lib/python2.7/site-packages/django/contrib/admin/static/',
    os.path.join(BASE_DIR, "static"),
)

STATIC_ROOT = '/home/admin/escca/releases/current/static/'

MEDIA_ROOT = '/var/www/escca/media/'


# Exceptions logging : 
# https://app.getsentry.com/openscop/escca
RAVEN_CONFIG = {
    'dsn': 'https://PUBLIC:SECRET@app.getsentry.com/ID',
}
