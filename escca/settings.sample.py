"""
Django settings for escca project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
from escca.settings_jenkins import *
from escca.settings_ckeditor import *
from escca.settings_endless_paginator import *

import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'CHANGE_THIS'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

SITE_ID = 1

# Email backend
# https://docs.djangoproject.com/en/1.6/topics/email/#email-backends
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = 'xxx'
#EMAIL_HOST = 'xxx'
#EMAIL_PORT = xxx
EMAIL_HOST_USER = 'xxx'
#EMAIL_HOST_PASSWORD = 'xxx'
#EMAIL_USE_TLS = True


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'raven.contrib.django.raven_compat',
    'allauth',
    'allauth.account',
    'bootstrap3_datetime',
    'ckeditor',
    'crispy_forms',
    'django_countries',
    'django_extensions',
    'django_fsm',
    'django_jenkins',
    'easy_pdf',
    'endless_pagination',
    'fsm_admin',
    'guardian',
    'cytometry',
    'directory',
    'quiz',
    'sponsors',
    'lectures',
    'tutorships',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)


# Template Context Processors
# https://docs.djangoproject.com/en/1.6/ref/settings/#template-context-processors
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'allauth.account.context_processors.account',
)

# Template Loaders : enable the cached loader for better performance
# https://docs.djangoproject.com/en/1.6/ref/templates/api/#django.template.loaders.cached.Loader
TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)


# Templates
# https://docs.djangoproject.com/en/1.6/ref/settings/#template-dirs
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]


# Authentication : how user can log in
# https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
)

# https://github.com/lukaszb/django-guardian#configuration
ANONYMOUS_USER_ID = -1
GUARDIAN_RENDER_403 = True

ROOT_URLCONF = 'escca.urls'

WSGI_APPLICATION = 'escca.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'


# Sets default template pack for django-crispy-forms
# http://django-crispy-forms.readthedocs.org/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap3'


# Account Settings (Managed by django-allauth)
# https://github.com/pennersr/django-allauth
ACCOUNT_SIGNUP_FORM_CLASS = 'quiz.forms.SignupForm'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True

JOIN_ESCCA_URL = "http://escca.eu/join"
