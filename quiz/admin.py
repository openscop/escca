import datetime

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _

from ckeditor.widgets import CKEditorWidget

from fsm_admin.mixins import FSMTransitionMixin

from guardian.admin import GuardedModelAdmin

from .models import Answer
from .models import Profile
from .models import Quiz
from .models import Question
from .models import Sitting


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 1


class QuizAdmin(FSMTransitionMixin, GuardedModelAdmin):
    inlines = [
        QuestionInline,
    ]
    list_display = (
        'title',
        'level',
        'topic',
        'state',
    )
    list_filter = (
        'level',
        'topic',
        'state',
    )
    readonly_fields = ('state', )


class ProfileInline(admin.StackedInline):
    model = Profile
    readonly_fields = ('fees_payed', )


class FeesPayedForCurrentYearListFilter(admin.SimpleListFilter):
    title = _('fees payed for current year')
    parameter_name = 'fees_payed_this_year'

    def lookups(self, request, model_admin):
        return(
            ('yes', _('yes')),
            ('no', _('no')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(profile__fees_payed__year=datetime.date.today().year)
        if self.value() == 'no':
            return queryset.exclude(profile__fees_payed__year=datetime.date.today().year)


class FeesPayedForNextYearListFilter(FeesPayedForCurrentYearListFilter):
    title = _('fees payed for next year')
    parameter_name = 'fees_payed_next_year'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(profile__fees_payed__year=datetime.date.today().year + 1)
        if self.value() == 'no':
            return queryset.exclude(profile__fees_payed__year=datetime.date.today().year + 1)


class ESCCAUserAdmin(UserAdmin):
    actions = [
        'fee_payed_for_current_year',
        'fee_payed_for_next_year',
    ]
    inlines = [
        ProfileInline,
    ]
    list_filter = (
        FeesPayedForCurrentYearListFilter,
        FeesPayedForNextYearListFilter,
        'profile__fees_payed',
        'profile__accept_emails',
    )

    def fee_payed_for_current_year(self, request, queryset):
        for user in queryset:
            user.profile.pay_fees_for_current_year()
        self.message_user(request, _("{} user fees marked as payed for current year".format(queryset.count())))
    fee_payed_for_current_year.short_description = _('mark fees of selected users payed for the year')

    def fee_payed_for_next_year(self, request, queryset):
        for user in queryset:
            user.profile.pay_fees_for_next_year()
        self.message_user(request, _("{} user fees marked as payed for next year".format(queryset.count())))
    fee_payed_for_next_year.short_description = _('mark fees of selected users payed for next year')


class AnswerInline(admin.TabularInline):
    model = Answer
    can_delete = False
    readonly_fields = ('question', 'answer', )
    extra = 0

    def has_add_permission(self, request):
        return False


class SittingAdmin(admin.ModelAdmin):
    inlines = [
        AnswerInline,
    ]


class MyFlatpageForm(FlatpageForm):
    content = forms.CharField(widget=CKEditorWidget())


class MyFlatPageAdmin(FlatPageAdmin):  # pylint: disable=R0904
    form = MyFlatpageForm


admin.site.unregister(FlatPage)
admin.site.unregister(Site)
admin.site.unregister(User)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(Sitting, SittingAdmin)
admin.site.register(User, ESCCAUserAdmin)
admin.site.register(FlatPage, MyFlatPageAdmin)
