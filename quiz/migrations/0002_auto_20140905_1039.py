# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='city',
            field=models.CharField(max_length=255, default=1, verbose_name='city'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='lab_name',
            field=models.CharField(max_length=255, default=1, blank=True, verbose_name='lab name'),
            preserve_default=False,
        ),
    ]
