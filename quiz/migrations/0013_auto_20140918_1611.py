# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0012_auto_20140917_1610'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='quiz',
            options={'verbose_name': 'quiz', 'verbose_name_plural': 'quizzes'},
        ),
    ]
