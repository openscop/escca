# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611
import django_fsm


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0015_auto_20140925_1722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quiz',
            name='state',
            field=django_fsm.FSMField(default='D', max_length=50, verbose_name='publication state',
                                      choices=[('D', 'draft'), ('N', 'needs review'), ('R', 'reviewed'),
                                               ('P', 'published'), ('E', 'expired')]),
        ),
    ]
