# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0007_auto_20141007_1625'),
        ('quiz', '0023_remove_profile_topics_of_interest'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileTopicsOfInterest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('profile', models.ForeignKey(to='quiz.Profile')),
                ('topic', models.ForeignKey(to='cytometry.Topic')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='profile',
            name='topics_of_interest',
            field=models.ManyToManyField(to='cytometry.Topic', through='quiz.ProfileTopicsOfInterest'),
            preserve_default=True,
        ),
    ]
