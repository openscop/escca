# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0024_auto_20141014_1026'),
    ]

    operations = [
        migrations.AddField(
            model_name='profiletopicsofinterest',
            name='is_reviewer',
            field=models.BooleanField(default=False, verbose_name='I would like to be part of reviewers team for this topic'),  # noqa
            preserve_default=True,
        ),
    ]
