# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0029_sitting_pre_quiz'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitting',
            name='post_quiz',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
