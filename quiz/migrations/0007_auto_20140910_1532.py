# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0006_profile_topics_of_interest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='topics_of_interest',
            field=models.ManyToManyField(blank=True, null=True, to='cytometry.Topic'),
        ),
    ]
