# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0003_auto_20140910_1116'),
        ('quiz', '0005_auto_20140910_1059'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='topics_of_interest',
            field=models.ManyToManyField(to='cytometry.Topic'),
            preserve_default=True,
        ),
    ]
