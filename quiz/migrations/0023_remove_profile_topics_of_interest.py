# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0022_auto_20141008_1530'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='topics_of_interest',
        ),
    ]
