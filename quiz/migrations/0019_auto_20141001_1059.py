# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0018_auto_20140929_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='answerer',
            field=models.BooleanField(default=False, help_text='\n                  You may be asked to answer a punctual question in your field.\n                  Question will be sent to you and you will have to answer by mail to the requester.\n\n                  For highly relevant question, you are encourage to submit a short report,\n                  letter and a new Quizz of interest, to be available for more cytometrists\n                  ', verbose_name='I accept answering questions from other members of the platform'),  # noqa
        ),
    ]
