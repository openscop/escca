# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611
import django_fsm


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0010_quiz_state'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quiz',
            name='state',
            field=django_fsm.FSMField(default='draft', max_length=50, verbose_name='publication state', choices=[('draft', 'draft'), ('published', 'published'), ('expired', 'expired')]),  # noqa
        ),
    ]
