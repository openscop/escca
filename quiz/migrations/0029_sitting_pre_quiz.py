# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0028_question_ponderation'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitting',
            name='pre_quiz',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
