from __future__ import unicode_literals

from django.contrib.auth.models import User

import factory

from cytometry.factories import TopicFactory

from .models import Quiz
from .models import Question


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User


class QuizFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Quiz

    title = factory.Sequence(lambda n: 'title_{0}'.format(n))
    topic = factory.SubFactory(TopicFactory)
    description = factory.Sequence(lambda n: 'description_{0}'.format(n))


class QuestionFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Question

    quiz = factory.SubFactory(QuizFactory)
    question = factory.Sequence(lambda n: 'question_{0}'.format(n))
    explanation = factory.Sequence(lambda n: 'explanation_{0}'.format(n))
