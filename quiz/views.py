from __future__ import unicode_literals

import datetime

from django.conf import settings

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from django.forms.formsets import formset_factory

from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from django.views.generic import View
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import FormView, UpdateView
from django.views.generic.list import ListView

# from django_fsm import TransitionNotAllowed
from braces.views import SuperuserRequiredMixin

from endless_pagination.views import AjaxListView

from easy_pdf.views import PDFTemplateView

from extra_views import CreateWithInlinesView
from extra_views import UpdateWithInlinesView

from guardian.mixins import PermissionRequiredMixin

from cytometry.models import Topic
from cytometry.models import SubField
from cytometry.models import Field

from sponsors.models import Sponsor

from .forms import AnswerForm
from .forms import ContactForm
from .forms import ProfileForm
from .forms import QuizForm
from .forms import QuestionInline
from .forms import TopicInline
from .forms import UserTopicsForm
from .forms import QuizCommentForm

from .models import Answer
from .models import Profile
from .models import Quiz
from .models import Sitting
from .models import State


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['sponsors'] = Sponsor.objects.displayable()
        if context['sponsors'].count() > 0:
            context['sponsor_columns'] = 12 / context['sponsors'].count()
        return context


class ProfileView(ListView):
    model = Topic
    template_name = 'profile.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        queryset = Topic.objects.filter(quiz__sitting__user=self.request.user).distinct()
        for topic in queryset:
            best_sittings = self.request.user.sitting_set.filter(quiz__topic=topic).only_best_scores()
            topic.right = best_sittings.overall_right()
            topic.wrong = best_sittings.overall_wrong()
            topic.unknown = best_sittings.overall_unknown()
            topic.score = best_sittings.overall_score()
            topic.right_percentage = best_sittings.overall_right_percentage()
            topic.wrong_percentage = best_sittings.overall_wrong_percentage()
            topic.unknown_percentage = best_sittings.overall_unknown_percentage()
        return queryset


class PPPView(PDFTemplateView):
    template_name = 'ppp.html'

    def get_context_data(self, **kwargs):
        context = super(PPPView, self).get_context_data(**kwargs)
        context['today'] = datetime.date.today()
        return context


class TopicResultsView(ListView):
    model = Sitting
    template_name = 'quiz/topic_results.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TopicResultsView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.request.user.sitting_set.filter(quiz__topic=self.kwargs['pk']).only_best_scores()


class ContactView(SuccessMessageMixin, FormView):
    template_name = 'quiz/contact_form.html'
    form_class = ContactForm
    success_url = '/'
    success_message = _('Your message has been successfully sent')

    def form_valid(self, form):
        form.send_email(self.request.POST['email'],
                        self.request.POST['subject'],
                        self.request.POST['message'])
        return super(ContactView, self).form_valid(form)


class ProfileUpdateView(UpdateView):
    model = Profile
    form_class = ProfileForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user.profile


class UserTopicsView(UpdateWithInlinesView):
    model = Profile
    inlines = [TopicInline]
    form_class = UserTopicsForm
    template_name_suffix = '_topics_update_form'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserTopicsView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user.profile


class QuizCreateView(CreateWithInlinesView):
    model = Quiz
    inlines = [QuestionInline]
    form_class = QuizForm
    template_name_suffix = '_create_form'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizCreateView, self).dispatch(*args, **kwargs)

    def forms_valid(self, form, inlines):
        response = super(QuizCreateView, self).forms_valid(form, inlines)
        form.instance.authors.add(self.request.user)
        return response


class QuizUpdateView(PermissionRequiredMixin, UpdateWithInlinesView):
    permission_required = 'quiz.change_quiz'
    return_403 = True
    model = Quiz
    inlines = [QuestionInline]
    form_class = QuizForm
    template_name_suffix = '_update_form'


class QuizAskReviewView(PermissionRequiredMixin, SingleObjectMixin, View):
    permission_required = 'quiz.change_quiz'
    return_403 = True
    model = Quiz

    def get(self, request, *args, **kwargs):
        quiz = self.get_object()
        quiz.ask_reviewer()
        quiz.save()
        return redirect(quiz)


class QuizAddReviewerView(SingleObjectMixin, View):
    model = Quiz

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizAddReviewerView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        quiz = self.get_object()
        if self.request.user.profile.is_possible_reviewer(quiz):
            quiz.add_reviewer(self.request.user)
            quiz.save()
        else:
            raise PermissionDenied
        return redirect(quiz)


class QuizPopReviewerView(PermissionRequiredMixin, SingleObjectMixin, View):
    permission_required = 'quiz.review_quiz'
    return_403 = True
    model = Quiz

    def post(self, request, *args, **kwargs):
        quiz = self.get_object()
        quiz.pop_reviewer(self.request.user)
        quiz.save()
        return redirect(quiz)


class QuizCommentView(PermissionRequiredMixin, SuccessMessageMixin, SingleObjectMixin, FormView):
    permission_required = 'quiz.review_quiz'
    return_403 = True
    model = Quiz
    template_name = 'quiz/quiz_comment_form.html'
    form_class = QuizCommentForm
    success_message = _('Your comment has been successfully sent to the author')

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(QuizCommentView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(QuizCommentView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        self.object.send_comment_to_author(self.request.POST['message'], self.request.user.email)
        return super(QuizCommentView, self).form_valid(form)

    def get_success_url(self):
        return reverse('quiz:quiz-detail', kwargs={'pk': self.object.pk})


class QuizReviewView(PermissionRequiredMixin, SingleObjectMixin, View):
    permission_required = 'quiz.review_quiz'
    return_403 = True
    model = Quiz

    def get(self, request, *args, **kwargs):
        quiz = self.get_object()
        quiz.review()
        quiz.save()
        return redirect(quiz)


class QuizPublishView(PermissionRequiredMixin, SingleObjectMixin, View):
    permission_required = 'quiz.change_quiz'
    return_403 = True
    model = Quiz

    def get(self, request, *args, **kwargs):
        quiz = self.get_object()
        quiz.publish()
        quiz.save()
        return redirect(quiz)


class QuizArchiveView(PermissionRequiredMixin, SingleObjectMixin, View):
    permission_required = 'quiz.change_quiz'
    return_403 = True
    model = Quiz

    def get(self, request, *args, **kwargs):
        quiz = self.get_object()
        quiz.archive()
        quiz.save()
        return redirect(quiz)


class QuizListView(AjaxListView):
    model = Quiz

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Quiz.objects.filter(topic__in=self.request.user.profile.topics_of_interest.all()).published()


class QuizUserListView(AjaxListView):
    model = Quiz
    template_name_suffix = '_user_list'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizUserListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.request.user.quiz_authored.all()


class QuizReviewListView(AjaxListView):
    model = Quiz
    template_name_suffix = '_review_list'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizReviewListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Quiz.objects.filter(
            topic__in=self.request.user.profile.topics_of_interest.filter(
                profiletopicsofinterest__is_reviewer=True
            ),
            state=State.PENDING_REVIEWER,
        ).exclude(authors=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(QuizReviewListView, self).get_context_data(**kwargs)
        context['pending_review_quizzes'] = self.request.user.quiz_reviewed.filter(state='pending_review')
        return context


class QuizByTopicListView(AjaxListView):
    template_name_suffix = '_by_topic_list'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizByTopicListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        topic = get_object_or_404(Topic, slug=self.kwargs['topicslug'])
        return Quiz.objects.filter(topic=topic).published()

    def get_context_data(self, **kwargs):
        context = super(QuizByTopicListView, self).get_context_data(**kwargs)
        topic = get_object_or_404(Topic, slug=self.kwargs['topicslug'])
        context['topic'] = topic
        context['subfield'] = topic.subfield
        context['field'] = topic.subfield.field
        return context


class QuizDetailView(DetailView):
    model = Quiz

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(QuizDetailView, self).get_context_data(**kwargs)
        context['possible_reviewer'] = self.request.user.profile.is_possible_reviewer(context['quiz'])
        return context


class QuizResultView(DetailView):
    model = Quiz
    template_name = 'quiz/quiz_result.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizResultView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        '''
        Here, we must get the sitting with the best score, excluding first attempts
        at lectures
        '''
        quiz = super(QuizResultView, self).get_object(queryset=None)
        sittings = quiz.sitting_set.filter(user=self.request.user).exclude(lecturesitting__first_attempt=True)
        return sorted([[sitting.score(), sitting] for sitting in sittings], reverse=True)[0][1]


class QuizTakeView(FormView):
    model = Sitting
    template_name = 'quiz/quiz_take_form.html'
    formset_class = AnswerForm
    extra = 0

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuizTakeView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(QuizTakeView, self).get_context_data(**kwargs)
        context['quiz'] = self.get_quiz()
        return context

    def get_quiz(self):
        return get_object_or_404(Quiz, pk=self.kwargs['pk'])

    def get_initial(self, sitting):
        return [{
            'question': question,
            'ponderation': question.ponderation,
            'sitting': sitting,
        } for question in sitting.quiz.question_set.all()]

    def get_formset_kwargs(self, sitting):
        kwargs = {}
        initial = self.get_initial(sitting)
        if initial:
            kwargs['initial'] = initial

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def construct_formset(self):
        sitting = self.model(user=self.request.user, quiz=self.get_quiz())
        formset_class = formset_factory(self.formset_class, extra=self.extra)
        formset = formset_class(**self.get_formset_kwargs(sitting))
        return formset, sitting

    def allready_taken(self):
        return self.model.objects.filter(
            user=self.request.user,
            quiz=self.get_quiz(),
            created__gte=datetime.date.today() - datetime.timedelta(weeks=4)
        ).exists()

    def assert_user_has_payed_fees(self):
        if not self.request.user.profile.fees_payed_for_current_year():
            messages.add_message(
                self.request,
                messages.WARNING,
                mark_safe(_(
                    """In order to pass the quiz, you need to pay the membership fees.
                       To do so, <a href="{}">please follow this link</a>""".format(settings.JOIN_ESCCA_URL)
                ))
            )
            return redirect('quiz:quiz-detail', pk=self.kwargs['pk'])

    def assert_user_has_not_taken_quiz(self):
        if self.allready_taken():
            messages.add_message(
                self.request,
                messages.WARNING,
                _('You allready passed this quiz. '
                  'To pass this quiz again, a delay of 4 weeks is required')
            )
            return redirect('quiz:quiz-list')

    def get(self, request, *args, **kwargs):
        self.assert_user_has_payed_fees()
        self.assert_user_has_not_taken_quiz()
        formset, _sitting = self.construct_formset()
        return self.render_to_response(self.get_context_data(formset=formset))

    def get_success_url(self):
        return redirect('quiz:quiz-result', pk=self.get_quiz().pk)

    def formset_valid(self, formset):
        for form in formset:
            Answer.objects.update_or_create(
                sitting=form.initial['sitting'],
                question=form.initial['question'],
                defaults={'answer': form.cleaned_data['answer']}
            )
        return self.get_success_url()

    def post(self, request, *args, **kwargs):
        # We remove the old sitting
        self.model.objects.filter(
            user=request.user,
            quiz=self.get_quiz(),
            created__lt=datetime.date.today() - datetime.timedelta(weeks=4)
        ).delete()
        formset, sitting = self.construct_formset()
        sitting.save()
        if formset.is_valid():
            return self.formset_valid(formset)


class GeneralStatsView(TemplateView):
    template_name = 'quiz/stats.html'

    def get_context_data(self, **kwargs):
        context = super(GeneralStatsView, self).get_context_data(**kwargs)
        context['sittings_by_field'] = [
            (field, Sitting.objects.filter(quiz__topic__subfield__field=field))
            for field in Field.objects.all()
        ]
        context['users'] = User.objects.all().count()
        context['quiz_published'] = Quiz.objects.published().count()
        context['quiz_passed'] = Sitting.objects.all().count()
        return context


class StatsBySubFieldView(DetailView):
    model = Field
    template_name = 'quiz/stats_by_subfield.html'

    def get_context_data(self, **kwargs):
        context = super(StatsBySubFieldView, self).get_context_data(**kwargs)
        context['sittings_by_subfield'] = [
            (subfield, Sitting.objects.filter(quiz__topic__subfield=subfield))
            for subfield in SubField.objects.filter(field=self.get_object())
        ]
        return context


class StatsByTopicView(DetailView):
    model = SubField
    template_name = 'quiz/stats_by_topic.html'

    def get_context_data(self, **kwargs):
        context = super(StatsByTopicView, self).get_context_data(**kwargs)
        context['sittings_by_topic'] = [
            (topic, Sitting.objects.filter(quiz__topic=topic))
            for topic in Topic.objects.filter(subfield=self.get_object())
        ]
        return context


class StatsByQuizView(DetailView):
    model = Topic
    template_name = 'quiz/stats_by_quiz.html'

    def get_best_students(self, quiz):
        return sorted(
            [(sitting.score(), sitting) for sitting in Sitting.objects.filter(quiz=quiz)],
            reverse=True,
        )[:3]

    def get_context_data(self, **kwargs):
        context = super(StatsByQuizView, self).get_context_data(**kwargs)
        context['sittings_by_quiz'] = [
            (quiz, Sitting.objects.filter(quiz=quiz), self.get_best_students(quiz=quiz))
            for quiz in Quiz.objects.filter(topic=self.get_object())
        ]
        return context


class StatsByQuestionView(SuperuserRequiredMixin, DetailView):
    model = Quiz
    template_name = 'quiz/stats_by_question.html'

    def get_context_data(self, **kwargs):
        context = super(StatsByQuestionView, self).get_context_data(**kwargs)
        context['sittings'] = Sitting.objects.filter(quiz=self.get_object())
        context['answers'] = [(
            question,
            Answer.objects.filter(question=question),
            Answer.objects.filter(question=question).rights_percentage(question),
            Answer.objects.filter(question=question).wrongs_percentage(question),
            Answer.objects.filter(question=question).unknown_percentage(question),
        ) for question in self.get_object().question_set.all()]
        return context


###########################################################
# MISC
###########################################################
class MailToUsersView(SuperuserRequiredMixin, TemplateView):
    template_name = 'quiz/mail_to_users.html'

    def get_context_data(self, **kwargs):
        context = super(MailToUsersView, self).get_context_data(**kwargs)
        context['mails'] = ",".join(
            [user.email for user in User.objects.all()]
        )
        return context
