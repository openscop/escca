from django.test import TestCase

from .admin import TopicInline

from .factories import FieldFactory
from .factories import SubFieldFactory
from .factories import TopicFactory

from .models import Topic


class TopicAdminTest(TestCase):

    def test_question_inline(self):
        admin = TopicInline(None, None)
        self.assertEqual(admin.model, Topic)
        self.assertEqual(admin.extra, 1)


class FieldTests(TestCase):

    def test_str(self):
        field = FieldFactory()
        self.assertEqual(field.__str__(), field.name)


class SubFieldTests(TestCase):

    def test_str(self):
        subfield = SubFieldFactory()
        self.assertEqual(subfield.__str__(), subfield.name)


class TopicTests(TestCase):

    def test_str(self):
        topic = TopicFactory()
        self.assertEqual(topic.__str__(), topic.name)
