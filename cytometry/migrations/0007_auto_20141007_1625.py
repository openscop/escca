# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0006_auto_20141001_1109'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='field',
            options={'ordering': ['name'], 'verbose_name': 'field'},
        ),
        migrations.AlterModelOptions(
            name='subfield',
            options={'ordering': ['name'], 'verbose_name': 'sub-field'},
        ),
        migrations.AlterModelOptions(
            name='topic',
            options={'ordering': ['name'], 'verbose_name': 'topic'},
        ),
    ]
