# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611
import django_extensions.db.fields


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0003_auto_20140910_1116'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(default='prout', populate_from='name', editable=False, blank=True, verbose_name='slug'),  # noqa
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subfield',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(default='prout', populate_from='name', editable=False, blank=True, verbose_name='slug'),  # noqa
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='topic',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(default='prout', populate_from='name', editable=False, blank=True, verbose_name='slug'),  # noqa
            preserve_default=False,
        ),
    ]
