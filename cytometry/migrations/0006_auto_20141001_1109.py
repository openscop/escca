# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
def generate_field_slugs(apps, schema_editor):
    Field = apps.get_model("cytometry", "Field")
    for field in Field.objects.all():
        field.save()


def generate_subfield_slugs(apps, schema_editor):
    SubField = apps.get_model("cytometry", "SubField")
    for subfield in SubField.objects.all():
        subfield.save()


def generate_topic_slugs(apps, schema_editor):
    Topic = apps.get_model("cytometry", "Topic")
    for topic in Topic.objects.all():
        topic.save()


class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0005_auto_20141001_1109'),
    ]

    operations = [
        migrations.RunPython(generate_field_slugs),
        migrations.RunPython(generate_subfield_slugs),
        migrations.RunPython(generate_topic_slugs),
    ]
