# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubField',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=255, verbose_name='name', unique=True)),
                ('field', models.ForeignKey(to='cytometry.Field', verbose_name='field')),
            ],
            options={
                'verbose_name': 'field',
            },
            bases=(models.Model,),
        ),
    ]
