from __future__ import unicode_literals

from django.conf.urls import url

from .views import FieldListView
from .views import SubFieldListView
from .views import TopicListView

urlpatterns = [
    url(r'^$', FieldListView.as_view(), name='field-list'),
    url(r'^(?P<slug>[-_\w]+)/$', SubFieldListView.as_view(), name='subfield-list'),
    url(r'^(?P<fieldslug>[-_\w]+)/(?P<subfieldslug>[-_\w]+)$', TopicListView.as_view(), name='topic-list'),
]
