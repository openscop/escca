from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator

from endless_pagination.views import AjaxListView

from .models import Field
from .models import SubField
from .models import Topic


class FieldListView(AjaxListView):
    model = Field

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FieldListView, self).dispatch(*args, **kwargs)


class SubFieldListView(AjaxListView):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SubFieldListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        field = get_object_or_404(Field, slug=self.kwargs['slug'])
        return SubField.objects.filter(field=field)

    def get_context_data(self, **kwargs):
        context = super(SubFieldListView, self).get_context_data(**kwargs)
        field = get_object_or_404(Field, slug=self.kwargs['slug'])
        context['field'] = field
        return context


class TopicListView(AjaxListView):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TopicListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        subfield = get_object_or_404(SubField, slug=self.kwargs['subfieldslug'])
        return Topic.objects.filter(subfield=subfield)

    def get_context_data(self, **kwargs):
        context = super(TopicListView, self).get_context_data(**kwargs)
        subfield = get_object_or_404(SubField, slug=self.kwargs['subfieldslug'])
        context['field'] = subfield.field
        context['subfield'] = subfield
        return context
