from __future__ import unicode_literals

from django.conf.urls import url

from .views import SponsorDetailView
from .views import SponsorListView


urlpatterns = [
    url(r'^$', SponsorListView.as_view(), name='sponsor-list'),
    url(r'^(?P<pk>\d+)$', SponsorDetailView.as_view(), name='sponsor-detail'),
]
