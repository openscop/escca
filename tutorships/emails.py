from __future__ import unicode_literals

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail


def get_current_site():
    return Site.objects.get_current()


def send_tutorship_accepted(tutoree, tutor, tutorship):
    current_site = get_current_site()
    subject = "[{}] Tutoring supervision accepted".format(current_site.name)
    body = (
        "{tutor_first_name} {tutor_last_name} has accepted to supervise your "
        "tutoring : {tutorship}\n"
        "Please take contact with him in order to organize your work together "
        "and the progress of your tutoring\n"
        "Your tutor's email is the following : {tutor_email}\n\n"
        "At the end of each step, a mail will be sent to you in order "
        "to evaluate your relation with your tutor\n"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n".format(
            tutor_first_name=tutor.first_name.title(),
            tutor_last_name=tutor.last_name.title(),
            tutorship=tutorship,
            tutor_email=tutor.email,
            site_domain=current_site.domain,
        )
    )
    send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [tutoree.email])


def confirm_tutorship_accepted(tutoree, tutor, tutorship):
    current_site = get_current_site()
    subject = "[{}] Tutoring supervision accepted".format(current_site.name)
    body = (
        "You accepted to supervise {tutoree_first_name} {tutoree_last_name} "
        "tutoring : {tutorship}\n"
        "Please take contact with him in order to organize your work together "
        "and the progress of the tutoring\n"
        "Your tutoree's email is the following : {tutoree_email}\n"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n".format(
            tutoree_first_name=tutoree.first_name.title(),
            tutoree_last_name=tutoree.last_name.title(),
            tutorship=tutorship,
            tutoree_email=tutoree.email,
            site_domain=current_site.domain,
        )
    )
    send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [tutor.email])


def send_tutorship_canceled(tutoree, tutor, tutorship):
    current_site = get_current_site()
    subject = "[{}] Tutoring supervision canceled".format(current_site.name)
    body = (
        "{tutor_first_name} {tutor_last_name} has canceled his supervision "
        "for your tutoring : {tutorship}.\n"
        "In order to continue your tutoring, you'll need to wait for another "
        "tutor to accept to supervise you."
        "\n\nGood luck\n"
        "--\n"
        "The {site_domain} Team\n".format(
            tutor_first_name=tutor.first_name.title(),
            tutor_last_name=tutor.last_name.title(),
            tutorship=tutorship,
            site_domain=current_site.domain,
        )
    )
    send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [tutoree.email])


def request_for_evaluation(tutoree, step):
    current_site = get_current_site()
    form_link = "http://{site_domain}/tutorships/{tutorship}/steps/{step}/evaluation/create/".format(
        site_domain=current_site.domain,
        tutorship=step.tutorship.id,
        step=step.id,
    )
    subject = "[{}] {} : Evaluation of step \"{}\"".format(current_site.name,
                                                           step.tutorship,
                                                           step)
    body = (
        "Step '{step}' has reached its end. \n"
        "In order to evaluate the quality of your working relation with your "
        "tutor, could you fill in the following form by clicking this link ? {form_link} \n"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n".format(
            step=step,
            form_link=form_link,
            site_domain=current_site.domain,
        )
    )
    send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [tutoree.email])
