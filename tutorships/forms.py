from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from crispy_forms.layout import Submit

from .models import Step
from .models import Tutorship
from .models import Evaluation


class TutorshipFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(TutorshipFormHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-horizontal'
        self.label_class = 'col-sm-3'
        self.field_class = 'col-sm-9'
        self.form_tag = True


class TutorshipForm(forms.ModelForm):

    class Meta:
        model = Tutorship
        fields = ['title', 'description', 'topic']

    def __init__(self, *args, **kwargs):
        super(TutorshipForm, self).__init__(*args, **kwargs)
        self.helper = TutorshipFormHelper()
        self.helper.add_input(Submit('submit', _('Submit')))


class TutorshipAcceptForm(forms.ModelForm):

    class Meta:
        model = Tutorship
        fields = []

    def __init__(self, *args, **kwargs):
        super(TutorshipAcceptForm, self).__init__(*args, **kwargs)
        self.helper = TutorshipFormHelper()
        self.helper.add_input(Submit('submit', _('I Accept')))


class TutorshipCancelForm(forms.ModelForm):

    class Meta:
        model = Tutorship
        fields = []

    def __init__(self, *args, **kwargs):
        super(TutorshipCancelForm, self).__init__(*args, **kwargs)
        self.helper = TutorshipFormHelper()
        self.helper.add_input(Submit('submit', _("I can't supervise this tutoring anymore")))


class StepForm(forms.ModelForm):

    class Meta:
        model = Step
        fields = ['title', 'description', 'duration']

    def __init__(self, *args, **kwargs):
        super(StepForm, self).__init__(*args, **kwargs)
        self.helper = TutorshipFormHelper()
        self.helper.layout = Layout(
            'title',
            'description',
            AppendedText('duration', 'weeks', active=True),
        )
        self.helper.add_input(Submit('submit', _('Submit')))


class EvaluationForm(forms.ModelForm):

    class Meta:
        model = Evaluation
        fields = ['satisfaction', 'comment']

    def __init__(self, *args, **kwargs):
        super(EvaluationForm, self).__init__(*args, **kwargs)
        self.helper = TutorshipFormHelper()
        self.helper.layout = Layout(
            AppendedText('satisfaction', '%', active=True),
            'comment',
        )
        self.helper.add_input(Submit('submit', _('Submit')))
