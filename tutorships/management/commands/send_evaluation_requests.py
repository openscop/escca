from __future__ import unicode_literals

from django.core.management.base import NoArgsCommand

from tutorships.emails import request_for_evaluation
from tutorships.models import Step


class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        for step in Step.objects.deadline_passed():
            request_for_evaluation(step.tutorship.tutoree, step)
        Step.objects.deadline_passed().update(evaluation_request_sent=True)
