from __future__ import unicode_literals

from django.conf.urls import url

from .views import TutorshipCreateView
from .views import TutorshipDetailView
from .views import TutorshipDeleteView
from .views import TutorshipListView
from .views import TutorshipManageListView
from .views import TutorshipUpdateView
from .views import TutorshipAcceptView
from .views import TutorshipCancelView

from .views import StepCreateView
from .views import StepDeleteView
from .views import StepUpdateView

from .views import EvaluationCreateView


urlpatterns = [
    url(r'^$', TutorshipListView.as_view(), name='tutorship-list'),
    url(r'^committee_panel/$', TutorshipManageListView.as_view(), name='tutorship-committee-list'),
    url(r'^ask$', TutorshipCreateView.as_view(), name='tutorship-create'),
    url(r'^(?P<pk>\d+)/$', TutorshipDetailView.as_view(), name='tutorship-detail'),
    url(r'^(?P<pk>\d+)/delete/$', TutorshipDeleteView.as_view(), name='tutorship-delete'),
    url(r'^(?P<pk>\d+)/update/$', TutorshipUpdateView.as_view(), name='tutorship-update'),
    url(r'^(?P<pk>\d+)/accept/$', TutorshipAcceptView.as_view(), name='tutorship-accept'),
    url(r'^(?P<pk>\d+)/cancel/$', TutorshipCancelView.as_view(), name='tutorship-cancel'),
    url(r'^(?P<tutorship>\d+)/steps/create/$', StepCreateView.as_view(), name='step-create'),
    url(r'^(?P<tutorship>\d+)/steps/(?P<pk>\d+)/delete/$', StepDeleteView.as_view(), name='step-delete'),
    url(r'^(?P<tutorship>\d+)/steps/(?P<pk>\d+)/update/$', StepUpdateView.as_view(), name='step-update'),
    url(r'^(?P<tutorship>\d+)/steps/(?P<step>\d+)/evaluation/create/$',
        EvaluationCreateView.as_view(), name='evaluation-create'),
]
