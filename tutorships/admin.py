from __future__ import unicode_literals

from django.contrib import admin

from .models import Tutorship
from .models import Step


class StepInline(admin.TabularInline):
    model = Step
    extra = 1


class TutorshipAdmin(admin.ModelAdmin):
    inlines = [
        StepInline,
    ]
    search_fields = [
        'title',
        'tutoree',
        'tutor',
        'step__set__title',
        'topic',
    ]


admin.site.register(Tutorship, TutorshipAdmin)
