# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0004_step_duration'),
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('satisfaction', models.PositiveIntegerField(verbose_name='satisfaction')),
                ('comment', models.TextField(verbose_name='comment')),
                ('step', models.ForeignKey(verbose_name='evaluation', to='tutorships.Step')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='step',
            name='evaluation_request_sent',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
