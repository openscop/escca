# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0002_auto_20150421_1650'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tutorship',
            name='start',
            field=models.DateField(null=True, verbose_name='start date of the tutorship', blank=True),
            preserve_default=True,
        ),
    ]
