# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tutorships.models


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0005_auto_20150423_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evaluation',
            name='satisfaction',
            field=models.PositiveIntegerField(verbose_name='satisfaction',
                                              validators=[tutorships.models.validate_percentage]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='evaluation',
            name='step',
            field=models.ForeignKey(verbose_name='evaluation', to='tutorships.Step', unique=True),
            preserve_default=True,
        ),
    ]
