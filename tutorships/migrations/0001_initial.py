# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0007_auto_20141007_1625'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Step',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', ckeditor.fields.RichTextField(verbose_name='description')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tutorship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('start', models.DateTimeField(verbose_name='start date of the tutorship')),
                ('description', ckeditor.fields.RichTextField(verbose_name='description')),
                ('topic', models.ForeignKey(verbose_name='topic', to='cytometry.Topic')),
                ('tutor', models.ForeignKey(related_name='tutorships_followed_set', verbose_name='tutor', blank=True,
                                            to=settings.AUTH_USER_MODEL, null=True)),
                ('tutoree', models.ForeignKey(verbose_name='tutoree', blank=True, to=settings.AUTH_USER_MODEL,
                                              null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='step',
            name='tutorship',
            field=models.ForeignKey(verbose_name='tutorship', to='tutorships.Tutorship'),
            preserve_default=True,
        ),
    ]
