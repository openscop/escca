from __future__ import unicode_literals

import datetime

from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

from braces.views import LoginRequiredMixin
from braces.views import UserPassesTestMixin

from guardian.mixins import PermissionRequiredMixin

from .models import Tutorship
from .models import Step
from .models import Evaluation
from .forms import TutorshipForm
from .forms import TutorshipAcceptForm
from .forms import TutorshipCancelForm
from .forms import StepForm
from .forms import EvaluationForm


class TutorshipListView(LoginRequiredMixin, ListView):
    model = Tutorship

    def get_queryset(self):
        return Tutorship.objects.filter(tutor=None).exclude(tutoree=self.request.user)


class TutorshipManageListView(LoginRequiredMixin, ListView):
    model = Tutorship
    template_name_suffix = '_committee_list'


class TutorshipCreateView(UserPassesTestMixin, CreateView):
    model = Tutorship
    form_class = TutorshipForm
    raise_exception = True

    def test_func(self, user):
        try:
            user.profile
        except AttributeError:
            return False
        return user.profile.fees_payed_for_current_year()

    def form_valid(self, form):
        form.instance.tutoree = self.request.user
        response = super(TutorshipCreateView, self).form_valid(form)
        return response


class TutorshipDetailView(LoginRequiredMixin, DetailView):
    model = Tutorship

    def get_context_data(self, **kwargs):
        context = super(TutorshipDetailView, self).get_context_data(**kwargs)
        context['today'] = datetime.date.today()
        return context


class TutorshipDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'tutorships.delete_tutorship'
    return_403 = True
    model = Tutorship
    success_url = reverse_lazy('tutorships:tutorship-list')


class TutorshipUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'tutorships.change_tutorship'
    return_403 = True
    model = Tutorship
    form_class = TutorshipForm
    template_name_suffix = '_update_form'


class TutorshipAcceptView(LoginRequiredMixin, UpdateView):
    model = Tutorship
    form_class = TutorshipAcceptForm
    template_name_suffix = '_accept_form'

    def form_valid(self, form):
        form.instance.tutor = self.request.user
        form.instance.start = datetime.date.today()
        form.instance.inform_tutorship_accepted()
        return super(TutorshipAcceptView, self).form_valid(form)


class TutorshipCancelView(PermissionRequiredMixin, UpdateView):
    permission_required = 'tutorships.change_tutorship'
    return_403 = True
    model = Tutorship
    form_class = TutorshipCancelForm
    template_name_suffix = '_cancel_form'

    def form_valid(self, form):
        form.instance.remove_tutor()
        form.instance.tutor = None
        form.instance.start = None
        return super(TutorshipCancelView, self).form_valid(form)


class StepCreateView(LoginRequiredMixin, CreateView):
    model = Step
    form_class = StepForm

    def form_valid(self, form):
        form.instance.tutorship = self.get_context_data()['tutorship']
        return super(StepCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(StepCreateView, self).get_context_data(**kwargs)
        context['tutorship'] = get_object_or_404(Tutorship, pk=self.kwargs['tutorship'])
        return context


class StepDeleteView(LoginRequiredMixin, DeleteView):
    model = Step

    def get_success_url(self):
        return reverse('tutorships:tutorship-detail', kwargs={'pk': self.object.tutorship.pk})


class StepUpdateView(LoginRequiredMixin, UpdateView):
    model = Step
    form_class = StepForm
    template_name_suffix = '_update_form'


class EvaluationCreateView(LoginRequiredMixin, CreateView):
    model = Evaluation
    form_class = EvaluationForm

    def form_valid(self, form):
        form.instance.step = self.get_context_data()['step']
        return super(EvaluationCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EvaluationCreateView, self).get_context_data(**kwargs)
        context['step'] = get_object_or_404(Step, pk=self.kwargs['step'])
        context['tutorship'] = get_object_or_404(Tutorship, pk=self.kwargs['tutorship'])
        return context
