# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lectures', '0005_auto_20150211_1408'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='bench_practice',
            field=models.IntegerField(verbose_name='bench practice'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lecture',
            name='data_analysis',
            field=models.IntegerField(verbose_name='data analysis'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lecture',
            name='lectures',
            field=models.IntegerField(verbose_name='lecture hours'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lecture',
            name='level',
            field=models.CharField(max_length=1, verbose_name='level',
                                   choices=[('1', 'basic'), ('2', 'current'), ('3', 'advanced'), ('4', 'expert')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lecture',
            name='topic',
            field=models.ForeignKey(verbose_name='topic', to='cytometry.Topic'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='language',
            field=models.CharField(max_length=255, verbose_name='language'),
            preserve_default=True,
        ),
    ]
