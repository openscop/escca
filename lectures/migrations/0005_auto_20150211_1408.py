# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0007_auto_20141007_1625'),
        ('lectures', '0004_auto_20150209_1524'),
    ]

    operations = [
        migrations.AddField(
            model_name='lecture',
            name='bench_practice',
            field=models.IntegerField(null=True, verbose_name='bench practice', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lecture',
            name='data_analysis',
            field=models.IntegerField(null=True, verbose_name='data analysis', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lecture',
            name='lectures',
            field=models.IntegerField(null=True, verbose_name='lecture hours', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lecture',
            name='level',
            field=models.CharField(blank=True, max_length=1, verbose_name='level',
                                   choices=[('1', 'basic'), ('2', 'current'), ('3', 'advanced'), ('4', 'expert')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lecture',
            name='topic',
            field=models.ForeignKey(verbose_name='topic', blank=True, to='cytometry.Topic', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='language',
            field=models.CharField(max_length=255, verbose_name='language', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='registration',
            field=models.URLField(verbose_name='registration link', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='end',
            field=models.DateTimeField(verbose_name='end of session'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='place',
            field=models.TextField(verbose_name='location'),
            preserve_default=True,
        ),
    ]
