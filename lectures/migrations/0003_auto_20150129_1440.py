# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lectures', '0002_auto_20150129_1313'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='teachers',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, null=True, verbose_name='teachers', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='attendees',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, null=True, verbose_name='attendees', blank=True),
            preserve_default=True,
        ),
    ]
