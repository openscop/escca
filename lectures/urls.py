from __future__ import unicode_literals

from django.conf.urls import url

from .views import AttendeeCreateView
from .views import LectureCreateView
from .views import LectureDetailView
from .views import LectureDeleteView
from .views import LectureUpdateView
from .views import LectureUserListView
from .views import SessionListView
from .views import QuizAttachView
from .views import QuizDetachView
from .views import PreQuizTakeView
from .views import PostQuizTakeView
from .views import PostQuizResultView
from .views import QuizTeacherResultsView
from .views import SessionCreateView
from .views import SessionDeleteView
from .views import SessionUpdateView
from .views import TeacherCreateView
from .views import TeacherDeleteView


urlpatterns = [
    url(r'^$',
        SessionListView.as_view(), name='session-list'),
    url(r'^user_list$',
        LectureUserListView.as_view(), name='lecture-list'),
    url(r'^create$',
        LectureCreateView.as_view(), name='lecture-create'),
    url(r'^(?P<pk>\d+)/$',
        LectureDetailView.as_view(), name='lecture-detail'),
    url(r'^(?P<pk>\d+)/update/$',
        LectureUpdateView.as_view(), name='lecture-update'),
    url(r'^(?P<pk>\d+)/delete/$',
        LectureDeleteView.as_view(), name='lecture-delete'),
    url(r'^(?P<pk>\d+)/session/add$',
        SessionCreateView.as_view(), name='session-create'),
    url(r'^(?P<pk>\d+)/teacher/add$',
        TeacherCreateView.as_view(), name='teacher-create'),
    url(r'^(?P<pk>\d+)/quiz/add$',
        QuizAttachView.as_view(), name='quiz-attach'),
    url(r'^(?P<lecture>\d+)/teacher/(?P<pk>\d+)/delete/$',
        TeacherDeleteView.as_view(), name='teacher-delete'),
    url(r'^(?P<lecture>\d+)/quiz/(?P<pk>\d+)/delete/$',
        QuizDetachView.as_view(), name='quiz-detach'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<session>\d+)/quiz/(?P<pk>\d+)/pretake/$',
        PreQuizTakeView.as_view(), name='prequiz-take'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<session>\d+)/quiz/(?P<pk>\d+)/posttake/$',
        PostQuizTakeView.as_view(), name='postquiz-take'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<session>\d+)/quiz/(?P<pk>\d+)/result/$',
        PostQuizResultView.as_view(), name='quiz-result'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<session>\d+)/quiz/(?P<pk>\d+)/teacherresults/$',
        QuizTeacherResultsView.as_view(), name='quiz-teacher-results'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<pk>\d+)/delete/$',
        SessionDeleteView.as_view(), name='session-delete'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<pk>\d+)/update/$',
        SessionUpdateView.as_view(), name='session-update'),
    url(r'^(?P<lecture>\d+)/sessions/(?P<pk>\d+)/attendee/add/$',
        AttendeeCreateView.as_view(), name='attendee-create'),
]
