from __future__ import unicode_literals

from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from bootstrap3_datetime.widgets import DateTimePicker

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from quiz.models import Quiz

from .models import Lecture
from .models import Session


class LectureFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(LectureFormHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-horizontal'
        self.label_class = 'col-sm-3'
        self.field_class = 'col-sm-9'
        self.form_tag = True


class LectureForm(forms.ModelForm):

    class Meta:
        model = Lecture
        fields = ['title', 'description', 'topic', 'level',
                  'lectures', 'data_analysis', 'bench_practice']

    def __init__(self, *args, **kwargs):
        super(LectureForm, self).__init__(*args, **kwargs)
        self.helper = LectureFormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class SessionForm(forms.ModelForm):

    class Meta:
        model = Session
        fields = ['start', 'end', 'place', 'language', 'registration']
        widgets = {
            'start': DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "pickSeconds": False
                }
            ),
            'end': DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "pickSeconds": False
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super(SessionForm, self).__init__(*args, **kwargs)
        self.helper = LectureFormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class TeacherForm(forms.ModelForm):

    class Meta:
        model = Lecture.teachers.through
        fields = ['user']

    def __init__(self, lecture, *args, **kwargs):
        super(TeacherForm, self).__init__(*args, **kwargs)
        # Here, we exclude allready added teachers from the queryset
        teachers = Lecture.teachers.through.objects.filter(lecture=lecture).values("user")
        no_profiles = User.objects.filter(profile=None).values("id")
        self.fields['user'].queryset = User.objects.exclude(id__in=teachers).exclude(id__in=no_profiles)
        self.helper = LectureFormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class QuizAttachForm(forms.ModelForm):

    class Meta:
        model = Lecture.quizzes.through
        fields = ['quiz']

    def __init__(self, lecture, *args, **kwargs):
        super(QuizAttachForm, self).__init__(*args, **kwargs)
        # Here, we exclude allready added teachers from the queryset
        quizzes = Lecture.quizzes.through.objects.filter(lecture=lecture).values("quiz")
        self.fields['quiz'].queryset = Quiz.objects.filter(state='published').exclude(id__in=quizzes)
        self.helper = LectureFormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class AttendeeCreateForm(forms.ModelForm):

    class Meta:
        model = Session.attendees.through
        fields = ()

    def __init__(self, *args, **kwargs):
        super(AttendeeCreateForm, self).__init__(*args, **kwargs)
        self.helper = LectureFormHelper()
        self.helper.add_input(Submit('submit', _('Attend this session')))
