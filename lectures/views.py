from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.forms.formsets import formset_factory
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.utils import timezone
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from braces.views import LoginRequiredMixin
from guardian.mixins import PermissionRequiredMixin

from quiz.models import Answer
from quiz.models import Quiz
from quiz.views import QuizTakeView
from quiz.views import QuizResultView

from .forms import AttendeeCreateForm
from .forms import LectureForm
from .forms import SessionForm
from .forms import TeacherForm
from .forms import QuizAttachForm
from .models import Lecture
from .models import Session
from .models import LectureSitting


class LectureCreateView(LoginRequiredMixin, CreateView):
    model = Lecture
    form_class = LectureForm

    def form_valid(self, form):
        response = super(LectureCreateView, self).form_valid(form)
        form.instance.teachers.add(self.request.user)
        return response


class LectureDetailView(LoginRequiredMixin, DetailView):
    model = Lecture

    def get_context_data(self, **kwargs):
        context = super(LectureDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['fees_link'] = settings.JOIN_ESCCA_URL
        context['pre_quiz_displayed'] = self.get_object().quizzes.exclude(
            sitting__in=self.request.user.sitting_set.filter(
                lecturesitting__first_attempt=True,
                lecturesitting__session__lecture=self.kwargs['pk']
            )
        )
        context['post_quiz_taken'] = [sitting.quiz for sitting in self.request.user.sitting_set.filter(
            lecturesitting__first_attempt=False,
            lecturesitting__session__lecture=self.kwargs['pk'],
        )]
        return context


class LectureDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'lectures.delete_lecture'
    return_403 = True
    model = Lecture
    success_url = reverse_lazy('lectures:session-list')


class LectureUserListView(LoginRequiredMixin, ListView):
    model = Lecture

    def get_queryset(self):
        return Lecture.objects.filter(teachers=self.request.user)


class SessionListView(ListView):
    model = Session


class LectureUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'lectures.change_lecture'
    return_403 = True
    model = Lecture
    form_class = LectureForm
    template_name_suffix = '_update_form'


class SessionCreateView(LoginRequiredMixin, CreateView):
    model = Session
    form_class = SessionForm

    def form_valid(self, form):
        form.instance.lecture = get_object_or_404(Lecture, pk=self.kwargs['pk'])
        return super(SessionCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SessionCreateView, self).get_context_data(**kwargs)
        context['lecture'] = get_object_or_404(Lecture, pk=self.kwargs['pk'])
        return context


class SessionDeleteView(DeleteView):
    model = Session

    def get_success_url(self):
        return reverse_lazy('lectures:lecture-detail', kwargs={'pk': self.kwargs['lecture']})


class SessionUpdateView(UpdateView):
    model = Session
    form_class = SessionForm
    template_name_suffix = '_update_form'


class QuizAttachView(CreateView):
    model = Lecture.quizzes.through
    form_class = QuizAttachForm

    def form_valid(self, form):
        form.instance.lecture = get_object_or_404(Lecture, pk=self.kwargs['pk'])
        return super(QuizAttachView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(QuizAttachView, self).get_context_data(**kwargs)
        context['lecture'] = get_object_or_404(Lecture, pk=self.kwargs['pk'])
        return context

    def get_form(self, form_class):
        return form_class(lecture=self.kwargs['pk'], **self.get_form_kwargs())

    def get_success_url(self):
        return reverse_lazy('lectures:lecture-detail', kwargs={'pk': self.kwargs['pk']})


class QuizDetachView(DeleteView):
    model = Lecture.quizzes.through

    def get_success_url(self):
        return reverse_lazy('lectures:lecture-detail', kwargs={'pk': self.kwargs['lecture']})

    def get_object(self, queryset=None):
        return get_object_or_404(Lecture.quizzes.through,
                                 lecture=self.kwargs['lecture'],
                                 quiz=self.kwargs['pk'])


class TeacherCreateView(CreateView):
    model = Lecture.teachers.through
    form_class = TeacherForm

    def form_valid(self, form):
        form.instance.lecture = get_object_or_404(Lecture, pk=self.kwargs['pk'])
        return super(TeacherCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TeacherCreateView, self).get_context_data(**kwargs)
        context['lecture'] = get_object_or_404(Lecture, pk=self.kwargs['pk'])
        return context

    def get_form(self, form_class):
        return form_class(lecture=self.kwargs['pk'], **self.get_form_kwargs())

    def get_success_url(self):
        return reverse_lazy('lectures:lecture-detail', kwargs={'pk': self.kwargs['pk']})


class TeacherDeleteView(DeleteView):
    model = Lecture.teachers.through

    def get_success_url(self):
        return reverse_lazy('lectures:lecture-detail', kwargs={'pk': self.kwargs['lecture']})

    def get_object(self, queryset=None):
        return get_object_or_404(Lecture.teachers.through,
                                 lecture=self.kwargs['lecture'],
                                 user=self.kwargs['pk'])


class AttendeeCreateView(LoginRequiredMixin, CreateView):
    model = Session.attendees.through
    form_class = AttendeeCreateForm

    def form_valid(self, form):
        form.instance.session = get_object_or_404(Session, pk=self.kwargs['pk'])
        form.instance.user = self.request.user
        return super(AttendeeCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AttendeeCreateView, self).get_context_data(**kwargs)
        context['session'] = get_object_or_404(Session, pk=self.kwargs['pk'])
        return context

    def get_success_url(self):
        return reverse_lazy('lectures:lecture-detail', kwargs={'pk': self.kwargs['lecture']})


class PreQuizTakeView(QuizTakeView):
    model = LectureSitting

    def get_lecture(self):
        return get_object_or_404(Lecture, pk=self.kwargs['lecture'])

    def get_session(self):
        return get_object_or_404(Session, pk=self.kwargs['session'])

    def get_context_data(self, **kwargs):
        context = super(PreQuizTakeView, self).get_context_data(**kwargs)
        context['lecture'] = self.get_lecture()
        return context

    def get_success_url(self):
        return redirect('lectures:lecture-detail', pk=self.get_lecture().pk)

    def allready_taken(self):
        return self.model.objects.filter(user=self.request.user,
                                         quiz=self.get_quiz(),
                                         first_attempt=True,
                                         session=self.get_session()).exists()

    def construct_formset(self):
        sitting = self.model(user=self.request.user,
                             quiz=self.get_quiz(),
                             session=self.get_session(),
                             first_attempt=True)
        formset_class = formset_factory(self.formset_class, extra=self.extra)
        formset = formset_class(**self.get_formset_kwargs(sitting))
        return formset, sitting


class PostQuizTakeView(PreQuizTakeView):

    def allready_taken(self):
        return self.model.objects.filter(user=self.request.user,
                                         quiz=self.get_quiz(),
                                         first_attempt=False,
                                         session=self.get_session()).exists()

    def construct_formset(self):
        sitting = self.model(user=self.request.user,
                             quiz=self.get_quiz(),
                             session=self.get_session())
        formset_class = formset_factory(self.formset_class, extra=self.extra)
        formset = formset_class(**self.get_formset_kwargs(sitting))
        return formset, sitting


class PostQuizResultView(QuizResultView):

    def get_session(self):
        return get_object_or_404(Session, pk=self.kwargs['session'])

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        queryset = queryset.filter(pk=pk)

        quiz = queryset.get()
        return quiz.sitting_set.get(user=self.request.user,
                                    lecturesitting__session=self.get_session(),
                                    lecturesitting__first_attempt=False)


class QuizTeacherResultsView(DetailView):
    model = Quiz
    template_name = 'lectures/quiz_teacher_results.html'

    def get_answers_for(self, question, sittings, first_attempt):
        return Answer.objects.filter(question=question,
                                     sitting__in=sittings,
                                     sitting__lecturesitting__first_attempt=first_attempt)

    def get_rights_for(self, question, sittings, first_attempt):
        answers = self.get_answers_for(question, sittings, first_attempt)
        rights = answers.filter(answer=question.correct).count()
        try:
            return rights * 100 / answers.count()
        except:
            return None

    def get_unknown_for(self, question, sittings, first_attempt):
        answers = self.get_answers_for(question, sittings, first_attempt)
        unknowns = answers.filter(answer=None).count()
        try:
            return unknowns * 100 / answers.count()
        except:
            return None

    def get_wrongs_for(self, question, sittings, first_attempt):
        answers = self.get_answers_for(question, sittings, first_attempt)
        wrongs = answers.exclude(answer=None).exclude(answer=question.correct).count()
        try:
            return wrongs * 100 / answers.count()
        except:
            return None

    def get_context_data(self, **kwargs):
        context = super(QuizTeacherResultsView, self).get_context_data(**kwargs)
        context['lecture'] = get_object_or_404(Lecture, pk=self.kwargs['lecture'])
        context['session'] = get_object_or_404(Session, pk=self.kwargs['session'])
        context['sittings'] = context['session'].lecturesitting_set.filter(quiz__pk=self.kwargs['pk'])
        if context['sittings'].count() > 0:
            results = [(
                question,
                self.get_rights_for(question, context['sittings'], True),
                self.get_unknown_for(question, context['sittings'], True),
                self.get_wrongs_for(question, context['sittings'], True),
                self.get_rights_for(question, context['sittings'], False),
                self.get_unknown_for(question, context['sittings'], False),
                self.get_wrongs_for(question, context['sittings'], False)
            ) for question in self.get_object().question_set.all()]
            context['result'] = results
        return context
